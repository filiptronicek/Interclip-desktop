# Interclip desktop
Interclip desktop client

Keyboard shortcuts:

 * ```Ctrl(Command)+C``` = copy code

(BETA) Download for Windows (click on image below or download from releases)


## Downloads

* Linux (.AppImage for Linux Ubuntu, RedHat, Debian, Fedora, Arch, whatever)
(for more support documentation, visit [appimage.org](https://appimage.org/))
![installing on Ubuntu](https://s.put.re/QKiEEMki.gif)

* Windows (.exe file)
![installing on Windows](https://s.put.re/uKny4NFr.gif)

* macOS (.dmg)
